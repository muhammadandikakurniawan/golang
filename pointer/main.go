package main

import "fmt"

type Address struct {
	City, Province, Country string
}

func (address *Address) SetCity(param string) {
	address.City = param
}

func SetProvince(address *Address, param string) {
	address.Province = param
}

func main() {

	// // var address1 Address = Address{"jakarta timur", "jakarta", "indonesia"}
	// // var address2 *Address = &address1

	// address1 := Address{"jakarta timur", "jakarta", "indonesia"}
	// address2 := &address1

	// // address2.City = "bali"
	// *address2 = Address{"jawa timur", "malang", "indonesia"}

	// address3 := address2

	// address4 := *address3

	// address4.City = "Bandung"

	// fmt.Println(address1)
	// fmt.Println(address2)
	// fmt.Println(address3)
	// fmt.Println(address4)

	address1 := Address{"Bandung", "Jawa Barat", "Indonesia"}

	address1.SetCity("Surabaya")

	SetProvince(&address1, "Jawa Timur")

	fmt.Println(address1)

}
