package entity

type Video struct {
	Title       string `json:"Title" binding:"min=10,max=100"`
	Description string `json:"Description" binding:"max=500,required"`
	Url         string `json:"Url" binding:"required"`
}
