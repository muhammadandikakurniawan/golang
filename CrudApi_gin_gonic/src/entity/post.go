package entity

type Post struct {
	Caption string      `json:"Caption" binding:"max=100,required"`
	Content PostContent `json:"Content" binding:"required"`
}

type PostContent struct {
	Type     int    `json:"Type" binding:"required"` //0 = image, 1 = video
	FileName string `json:"FileName" binding:"required"`
}
