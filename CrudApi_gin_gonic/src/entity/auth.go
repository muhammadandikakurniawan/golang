package entity

import "time"

type PoolUserToken struct {
	Id          string    `json:"Id" binding:"max=100,required"`
	CreatedDate time.Time `json:"CreatedDate"`
	UserName    string    `json:"UserName" binding:"required"`
	Token       string    `json:"Token" binding:"required"`
}
