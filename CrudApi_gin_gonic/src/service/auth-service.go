package service

import (
	"CrudApi_gin_gonic/src/common"
	"CrudApi_gin_gonic/src/entity"
	"CrudApi_gin_gonic/src/model"
	"context"
	"fmt"

	"encoding/json"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	uuid "github.com/nu7hatch/gouuid"
)

type IAuthService interface {
	GetToken(model.GetTokenParamModel) (model.GetTokenResultModel, error)
}

type AuthService struct {
}

func AuthServiceNew() *AuthService {
	return &AuthService{}
}

func (service *AuthService) GetToken(param model.GetTokenParamModel) (model.GetTokenResultModel, error) {
	result := &model.GetTokenResultModel{}
	tokenExpiresAt := time.Now().Add(time.Minute * 45).Unix()
	ctx := context.Background()
	authClaims := model.AuthTokenClaimsModel{
		StandardClaims: jwt.StandardClaims{
			Issuer:    "go_api",
			ExpiresAt: tokenExpiresAt,
		},
		Username: param.Username,
		Email:    param.Email,
		Password: param.Password,
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, authClaims)

	signToken, err := token.SignedString([]byte("ApiJWTSignatureKey"))

	if err == nil {
		result.Token = signToken
		result.Username = param.Username
	}

	tokenId, errTokenId := uuid.NewV4()

	if errTokenId != nil {
		fmt.Println("Error token id : ", errTokenId)
		panic("Error token id")
	}

	dataPoolUserToken := entity.PoolUserToken{
		Id:          tokenId.String(),
		UserName:    param.Username,
		CreatedDate: time.Now(),
		Token:       signToken,
	}

	esClient, errEsClient := common.GetESClient()

	dataJSON, errJson := json.Marshal(dataPoolUserToken)

	if errJson != nil {
		fmt.Println("Error errJson : ", errJson)
		panic("Error errJson")
	}

	if errEsClient != nil {
		fmt.Println("Error ES Client : ", errEsClient)
		panic("Error ES Client")
	}

	jsonPoolTokenBody := string(dataJSON)

	fmt.Println(jsonPoolTokenBody)

	insertTokenRes, insertTokenResErr := esClient.Index().Index("gin_pool_user_token").Type("_doc").Id(tokenId.String()).BodyJson(jsonPoolTokenBody).Do(ctx)
	if insertTokenResErr != nil {
		fmt.Println("Error insert pool token : ", insertTokenResErr)
		panic("Error insert pool token")
	}
	fmt.Println(insertTokenRes)

	return *result, err
}
