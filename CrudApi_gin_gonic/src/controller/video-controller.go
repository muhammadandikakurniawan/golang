package controller

import (
	"CrudApi_gin_gonic/src/entity"
	"CrudApi_gin_gonic/src/service"

	"github.com/gin-gonic/gin"
)

type IVideoController interface {
	FindAll() []entity.Video
	Save(ctx *gin.Context) entity.Video
}

type VideoController struct {
	service *service.VideoService
}

func (c *VideoController) FindAll() []entity.Video {
	return c.service.FindAll()
}

func (c *VideoController) Save(ctx *gin.Context) entity.Video {
	var video = entity.Video{}
	ctx.BindJSON(&video)
	c.service.Save(video)

	return video
}

func VideoControllerNew(service *service.VideoService) IVideoController {
	return &VideoController{
		service: service,
	}
}
