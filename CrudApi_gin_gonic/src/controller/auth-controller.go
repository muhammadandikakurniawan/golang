package controller

import (
	"CrudApi_gin_gonic/src/model"
	"CrudApi_gin_gonic/src/service"
	"fmt"

	"github.com/gin-gonic/gin"
)

type IAuthController interface {
	GetToken(ctx *gin.Context)
}

type AuthController struct {
	authService *service.AuthService
}

func AuthControllerNew(authService *service.AuthService) *AuthController {
	return &AuthController{
		authService: authService,
	}
}

func (controller *AuthController) GetToken(ctx *gin.Context) {

	statusCode := 200

	getTokenParam := model.GetTokenParamModel{}
	ctx.BindJSON(&getTokenParam)
	result, err := controller.authService.GetToken(getTokenParam)
	fmt.Println(err)
	if err != nil {
		statusCode = 500
	}

	ctx.JSON(statusCode, result)
}
