package middlewares

import (
	"fmt"

	"github.com/gin-gonic/gin"
)

func CustomAuth() gin.HandlerFunc {

	return func(ctx *gin.Context) {
		token := ctx.Request.Header.Get("ApiKey")

		fmt.Println(token)

		if token == "" {
			ctx.AbortWithStatusJSON(401, "Api key is required")
			return
		}
		if token != "apikey123" {
			ctx.AbortWithStatusJSON(401, "Api key is not valid")
			return
		}

		ctx.Next()
	}

}
