package model

import (
	jwt "github.com/dgrijalva/jwt-go"
)

type GetTokenParamModel struct {
	Username string `json:"Username" binding:"min=10,max=100"`
	Email    string `json:"Email" binding:"max=500,required"`
	Password string `json:"Password" binding:"required"`
}

type GetTokenResultModel struct {
	Token    string `json:"Token" `
	Username string `json:"Username" `
}

type AuthTokenClaimsModel struct {
	jwt.StandardClaims
	Username string `json:"Username" binding:"min=10,max=100"`
	Email    string `json:"Email" binding:"max=500,required"`
	Password string `json:"Password" binding:"required"`
}
