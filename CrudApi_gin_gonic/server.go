package main

import (
	"CrudApi_gin_gonic/src/controller"
	"CrudApi_gin_gonic/src/middlewares"
	"CrudApi_gin_gonic/src/service"
	"io"
	"os"

	"github.com/gin-gonic/gin"
)

var (
	videoService    *service.VideoService       = service.VideoServiceNew()
	videoController controller.IVideoController = controller.VideoControllerNew(videoService)

	authService    *service.AuthService       = service.AuthServiceNew()
	authController controller.IAuthController = controller.AuthControllerNew(authService)
)

func setupLogOutPut() {
	f, _ := os.Create("gin.log")

	gin.DefaultWriter = io.MultiWriter(f, os.Stdout)
}

func main() {
	setupLogOutPut()

	server := gin.New()

	routerGroup := server.RouterGroup

	// server.Use(
	// 	gin.Recovery(),
	// 	middlewares.Logger(),
	// 	middlewares.BasicAuth(),
	// )

	routerGroup.Use(
		gin.Recovery(),
		middlewares.Logger(),
		middlewares.BasicAuth(),
		middlewares.CustomAuth(),
	)

	VideoRoute := routerGroup.Group("api/Video")
	{
		VideoRoute.GET("/GetAll", func(ctx *gin.Context) {
			ctx.JSON(200, videoController.FindAll())
		})

		VideoRoute.POST("/Add", func(ctx *gin.Context) {
			ctx.JSON(200, videoController.Save(ctx))
		})
	}

	PostRoute := routerGroup.Group("api/Post")
	{
		PostRoute.GET("/GetAll", func(ctx *gin.Context) {
			ctx.JSON(200, videoController.FindAll())
		})

		PostRoute.POST("/Add", func(ctx *gin.Context) {
			ctx.JSON(200, videoController.Save(ctx))
		})
	}

	AuthRoute := routerGroup.Group("api/Auth")
	{
		AuthRoute.POST("/GetToken", authController.GetToken)
	}

	// server.GET("/test", func(ctx *gin.Context) {
	// 	ctx.JSON(200, gin.H{
	// 		"message": "OK!!",
	// 	})

	// })

	// server.GET("/greeting", func(ctx *gin.Context) {
	// 	param, ok := ctx.GetQuery("name")
	// 	res := ""
	// 	if ok {
	// 		res = "HI" + param
	// 	}
	// 	ctx.JSON(200, gin.H{
	// 		"message": res,
	// 	})

	// })

	// server.GET("/GetVideos", func(ctx *gin.Context) {

	// 	ctx.JSON(200, videoController.FindAll())

	// })

	// server.POST("/AddVideo", func(ctx *gin.Context) {
	// 	ctx.JSON(200, videoController.Save(ctx))

	// })
	server.Run(":212")
}
